FROM openjdk:8-jre-alpine

# Install Bash
RUN apk add --no-cache bash=4.4.19-r1

# Define Wildfly CLI Version and Download
ENV V_WILDFLY_CLI=18.0.4.Final
RUN wget -qnV -O /wildfly-cli.jar \
    https://repo1.maven.org/maven2/org/wildfly/core/wildfly-cli/$V_WILDFLY_CLI/wildfly-cli-$V_WILDFLY_CLI-client.jar \
    && wget -qP / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh

# Copy Skripts for Pipe to Image Root Directory
COPY pipe /

# Get it Going ;)
ENTRYPOINT ["/pipe.sh"]
