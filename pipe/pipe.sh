#!/bin/bash
#
# JBOSS CLI Deploy Pipe
#
# Required globals:
#   FILE
#
# Optional globals:
#   CONTROLLER
#   USER
#   PASSWORD
#   SERVER_GROUPS
#   NAME
#   DEBUG

source "$(dirname "$0")/common.sh"
source "$(dirname "$0")/functions.sh"

info "Starting pipe execution..."

# required parameters
FILE=${FILE:?'FILE variable missing.'}

# optional parameters
CONTROLLER=${CONTROLLER:=""}
USER=${USER:=""}
PASSWORD=${PASSWORD:=""}
SERVER_GROUPS=${SERVER_GROUPS:=""}
NAME=${NAME:=""}
DEBUG=${DEBUG:="false"}

# Resolve Variables & Initial Action

enable_debug

CONNECT_ARGS="--connect"
if [[ "${CONTROLLER}" != "" ]]; then
    CONNECT_ARGS="${CONNECT_ARGS} --controller=${CONTROLLER}"
fi
if [[ "${USER}" != "" ]]; then
    CONNECT_ARGS="${CONNECT_ARGS} --user=${USER}"
fi
if [[ "${PASSWORD}" != "" ]]; then
    CONNECT_ARGS="${CONNECT_ARGS} --password=${PASSWORD}"
fi
debug "Connection Args: $CONNECT_ARGS"

SERVER_GROUPS_ARGS=""
if [[ "${SERVER_GROUPS}" != "" ]]; then
  SERVER_GROUPS_ARGS="--server-groups=${SERVER_GROUPS}"
fi
debug "Server Groups Arg: $SERVER_GROUPS_ARGS"

if [[ "${NAME}" == "" ]]; then
  NAME="${FILE##*/}"
fi
debug "Deployment Name: $NAME"

run undeploy || true
run deploy && success Pipe Execution succeeded || fail Pipe Execution Failed
