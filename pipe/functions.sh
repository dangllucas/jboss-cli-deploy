#!/bin/bash

function deploy() {
    java -jar /wildfly-cli.jar \
      $CONNECT_ARGS \
      --command="deploy $SERVER_GROUPS_ARGS $FILE --name=$NAME"
}

function undeploy() {
    java -jar /wildfly-cli.jar \
      $CONNECT_ARGS \
      --command="undeploy --name=$NAME --all-relevant-server-groups"
}
